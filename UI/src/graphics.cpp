#include "../include/graphics.h"
#include <iostream>
    graphics::graphics(game* new_game, int res_x, int res_y)
        : new_game(new_game)
        , max_x(new_game->get_max_x())
        , max_y(new_game->get_max_y())
        , res_x(res_x)
        , res_y(res_y)
        , dir(dir_t::right)
        , window(sf::RenderWindow(sf::VideoMode(this->res_x, this->res_y), "SFML works!"))
        , shape(sf::RectangleShape({(static_cast<float>(this->res_x) / static_cast<float>(this->max_x)), (static_cast<float>(this->res_y) / static_cast<float>(this->max_y))}))
    {
        run();
    }

    void graphics::run()
    {         
        while (window.isOpen())
        {
            sf::Event event;
            sf::Clock clock;
            sf::Time timeSinceLastUpdate = sf::Time::Zero;
            window.clear();
            draw_dots();
            while(timeSinceLastUpdate.asMilliseconds() < 500)
            {
                get_input();
                timeSinceLastUpdate += clock.restart();
            }
            while (window.pollEvent(event))
            {
                if (event.type == sf::Event::Closed)
                {
                    window.close();
                }
                if (event.type == sf::Event::KeyPressed)
                {
                    
                }
            }
            if (!new_game->next_iteration(dir))
            {
                //draw_text("GAME OVER");
                //while(1);
                std::cout << "GAME OVER!\n";
                break;
            }
        }
    }

    void graphics::draw_dots()
    {
        for(int y = 0 ; y < max_y; y++)
        {
            for(int x = 0; x < max_x; x++)
            {
                //std::cout << "x = " << x << ", y = " << y << "\n";
                point dot = {x, y};
                float x_pos = (res_x / max_x) * x;
                float y_pos = (res_y / max_y) * (max_y - 1 - y);
                shape.setPosition({x_pos, y_pos});
                switch(new_game->get_dot_type(dot))
                {
                    case 1:
                        shape.setFillColor(sf::Color::Black);
                        break;
                    case 2:
                        shape.setFillColor(sf::Color::Green);
                        break;
                    case 8:
                        shape.setFillColor(sf::Color::Blue);
                        break;
                    default:
                        shape.setFillColor(sf::Color::White);
                }
                window.draw(shape);
            }
        }
        window.display();
    }


    bool graphics::get_input()
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            dir = dir_t::left;
            std::cout << "left!\n";
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            dir = dir_t::right;
            std::cout << "right!\n";
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            dir = dir_t::up;
            std::cout << "up!\n";
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            dir = dir_t::down;
            std::cout << "down!\n";
        }
        else
        {
            return false; // key not pressed
        }
        return true; // key pressed
    }