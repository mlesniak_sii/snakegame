#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/System/String.hpp>
#include "../../logic/include/game.h"
#include "../../logic/include/geometry.h"
#include "../include/graphics.h"
int main()
{
    std::cout << "Hello World!\n";    
    const int map_x = 5;
    const int map_y = 5;
    const int speed = 1;
    game new_game(speed, map_x, map_y);
    graphics UI(&new_game, 600, 600);
    
    return 0;
}

