#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/System/String.hpp>
#include "../../logic/include/game.h"
#include "../../logic/include/geometry.h"

class graphics
{
    public:
        graphics(game* new_game, int res_x, int res_y);
        void run();

    private:
        void draw_dots();
        bool get_input();

        game * new_game;
        int max_x;
        int max_y;
        int res_x;
        int res_y;
        dir_t dir;
        sf::RenderWindow window;
        sf::RectangleShape shape; 

    protected:

};
#endif