#include <iostream>
#include "../include/snake.h"

    snake::snake(vector map_size)
        : map_size(map_size)
        , max_size(map_size.x * map_size.y)
    {
        reset();
    }

    void snake::reset() // 3 jednostki
    {
        body.clear();
        point head = point{0, 0} + (map_size / 2);
        body.push_back(head);
        body.push_back(head + vector{-1, 0});
        body.push_back(head + vector{-2, 0});    
    }
    
    bool snake::attach_dot(point dot)
    {
        body.push_front(dot);
        return is_length_ok();
    }

    bool snake::is_length_ok() const
    {
        return !(body.size() == max_size);
    }
    
    bool snake::move(dir_t dir) 
    {
   
        body.push_front(body.front() + dir_to_vector(dir));
        body.pop_back();
        return !is_colision(body.front());
    }

    bool snake::is_colision(point pos) const
    { 
        vector zero = {0, 0};
        vector unit = {1, 1};
        if ((body.front() > map_size - unit) || (body.front() < zero))
        {
            return true;
        }
        if(body.size() > 4)
        {
            for(auto body_it = (body.begin() + 4); body_it != body.end(); ++body_it) // +4 zamiast next
            {
                if (*body_it == pos)
                {
                    return true;
                }
            }
        }
        return false;
    }

    bool snake::is_here(point pos) const
    { 
        for(auto body_it = (body.begin()); body_it != body.end(); ++body_it) 
        {
            if (*body_it == pos)
            {
                return true;
            }
        }
        return false;
    }

    point snake::get_head() const
    {
        return body.front();
    }