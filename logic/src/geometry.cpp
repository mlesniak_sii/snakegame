#include <utility>
#include "../include/geometry.h"

vector dir_to_vector(dir_t dir)
{
    switch(dir)
    {
        case dir_t::up: 
            return vector{0, 1};
        case dir_t::down:
            return vector{0, -1};
        case dir_t::left:
            return vector{-1, 0};
        case dir_t::right:
            return vector{1, 0};
        default: 
            return vector{0, 0}; //std::unreachable();
    }
}