#include <iostream>

#include "../include/board.h"
#include <random>

    board::board(int size_x, int size_y)
        : map_size({size_x, size_y})
        , player(snake(this->map_size))  
    {
        reset();
    }

    bool board::reset()
    {
        set_food();
        return is_map_valid();
    }

    point board::get_head() const
    {
        return player.get_head();
    }

    point board::get_food() const
    {
        return food;
    }

    bool board::move_snake(dir_t dir)
    {
        return player.move(dir);
    }

    void board::eat_food()
    {
        player.attach_dot(food);
    }
    

    bool board::is_map_valid() // private
    {
        if(map_size.x < MIN_MAP_SIZE || map_size.y < MIN_MAP_SIZE)
        {
            return false;
        }      
        return true;
        
    }

    bool board::set_food() // generate_food(), wektor pustych pol
    {
        if (!player.is_length_ok()) // private is_board_full()
        {
            return false;
        }
        std::random_device rd;  // a seed source for the random number engine, konstruktor
        std::mt19937 gen(rd()); // mersenne_twister_engine seeded with rd() // przeniesc do ciala klasy
        std::uniform_int_distribution<> distrib_x(0, map_size.x - 1);
        std::uniform_int_distribution<> distrib_y(0, map_size.y - 1);
        do
        {
            food.x = distrib_x(gen);
            food.y = distrib_y(gen);          
        }
        while (player.is_here(food));
        return true;
    }

    bool board::is_snake(point dot) const
    {
        return player.is_here(dot);
    }

    bool board::is_head(point dot) const
    {
        return (dot == player.get_head());
    }
     
    bool board::is_food(point dot) const
    {
        return (dot == food);
    }

    object_type board::get_dot_type(point dot)
    {
        if(is_snake(dot))
        {
            if (is_head(dot))
            {
                return object_type::head;
            }
            else
            {
                return object_type::tail;
            }
        }
        else if (is_food(dot))
        {
            return object_type::food;
        }
        else
        {
            return object_type::empty;
        }
    }
    
    int board::get_max_x()
    {
        return map_size.x;
    }
     
    int board::get_max_y()
    {
        return map_size.y;
    }