#include <iostream>
#include "../include/game.h"

    game::game(int speed, int size_x, int size_y)
        : speed(speed)
        , map(board(size_x, size_y))
    {
    }


    bool game::next_iteration(dir_t dir)
    {         
        if(map.get_head() + dir_to_vector(dir) == map.get_food())
        {
            map.eat_food();
            bool result = map.set_food();
            ++points;
            return result;
        }
        else
        {
            return map.move_snake(dir);
        }
    }

    int game::get_dot_type(point dot)
    {
        return static_cast<int>(map.get_dot_type(dot));
    }

    int game::get_points()
    {
        return points;
    }

    int game::get_max_x()
    {
        return map.get_max_x();
    }

    int game::get_max_y()
    {
        return map.get_max_y();
    }