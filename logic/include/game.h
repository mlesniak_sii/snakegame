#ifndef GAME_H
#define GAME_H
#include "board.h"
class game
{
    public:
        game(int speed, int size_x, int size_y);
        bool next_iteration(dir_t dir);
        int get_dot_type(point dot);// enum objetct_type get_object_type_on_position() const
        int get_points();
        int get_max_x();
        int get_max_y();
    private:
        int points = 0;
        int speed = 1;
        board map;
    protected:

};
#endif