#ifndef SNAKE_H
#define SNAKE_H
#include <deque>
#include "geometry.h"
class snake
{
    public:
        snake(vector map_size);
        void reset();// consty do funkcji
        bool attach_dot(point dot);
        bool move(dir_t dir);
        bool is_colision(point dot) const;
        bool is_length_ok() const;
        bool is_here(point pos) const;
        point get_head() const;
    private:
        std::deque<point> body;
        std::size_t max_size;
        vector map_size;
    protected:// pousuwac w klasach
    // cmake

};
#endif
