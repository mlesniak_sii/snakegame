#ifndef BOARD_H
#define BOARD_H
#include "geometry.h"
#include "snake.h"
#include <vector>
#include <random>
enum class object_type 
    {
        empty,
        tail,
        food,
        head = 8
    };
class board // game_state
{
    public:

        board(int size_x, int size_y);
        [[nodiscard]] bool is_map_valid();
        bool reset();
        bool set_food();
        point get_head() const;
        point get_food() const;
        bool move_snake(dir_t dir);
        void eat_food();
        bool is_snake(point dot) const;
        bool is_head(point dot) const;
        bool is_food(point dot) const;     
        object_type get_dot_type(point dot);   
        int get_max_x();
        int get_max_y();
    private:
        point food;
        vector map_size;   
        snake player;  
        const int MIN_MAP_SIZE = 5;
    protected:

};

#endif