#ifndef GEOMETRY_H
#define GEOMETRY_H


struct point
{
    int x = 0;
    int y = 0;

};

struct vector
{
    int x = 0;
    int y = 0;
};

inline bool operator==(point lhs, point rhs)
{   
    return (lhs.x == rhs.x) && (lhs.y == rhs.y);
}   

inline point operator/(point lhs, int rhs)
{   
    return point{lhs.x / rhs, lhs.y / rhs};
}   

inline point operator*(point lhs, int rhs)
{
    return point{lhs.x * rhs, lhs.y * rhs};
}

inline vector operator+(vector lhs, vector rhs)
{
    return vector{lhs.x + rhs.x, lhs.y + rhs.y};
}

inline vector operator-(vector lhs, vector rhs)
{
    return vector{lhs.x - rhs.x, lhs.y - rhs.y};
}

inline vector operator*(vector lhs, int rhs)
{
    return vector{lhs.x * rhs, lhs.y * rhs};
}

inline vector operator/(vector lhs, int rhs)
{
    return vector{lhs.x / rhs, lhs.y / rhs};
}

inline point operator+(point lhs, vector rhs)
{
    return point{lhs.x + rhs.x, lhs.y + rhs.y};
}

inline vector operator-(point lhs, point rhs)
{
    return vector{lhs.x - rhs.x, lhs.y - rhs.y};
}

inline bool operator==(vector lhs, vector rhs)
{
    return (lhs.x == rhs.x) && (lhs.y == rhs.y);
}

inline bool operator>(point lhs, vector rhs) // powinno byc contains(), struct rectangle{ point origin, vector size}
{
    return (lhs.x > rhs.x) || (lhs.y > rhs.y);
}

inline bool operator<(point lhs, vector rhs)
{
    return (lhs.x < rhs.x) || (lhs.y < rhs.y);
}

enum class dir_t 
{
    up,
    down,
    left,
    right
};

vector dir_to_vector(dir_t dir);

#endif