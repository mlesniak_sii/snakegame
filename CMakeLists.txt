cmake_minimum_required(VERSION 3.5)
 
project(SnakeGame)
 


add_executable(snakegame) 
target_sources(snakegame PRIVATE UI/src/main.cpp UI/src/graphics.cpp logic/src/board.cpp logic/src/snake.cpp logic/src/geometry.cpp logic/src/game.cpp)
find_package(SFML COMPONENTS window graphics system)
target_include_directories(snakegame PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/include)
target_link_libraries(snakegame	PUBLIC sfml-graphics sfml-window sfml-system)
target_compile_features(snakegame PUBLIC cxx_std_17)
target_compile_options(snakegame PRIVATE
		$<$<CXX_COMPILER_ID:GNU>:-Wall -Wextra -Wpedantic>
		$<$<CXX_COMPILER_ID:Clang>:-Wall -Wpedantic>
		$<$<CXX_COMPILER_ID:MSVC>:/W4>
)
